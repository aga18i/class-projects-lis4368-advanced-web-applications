> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Anthony Alangadan

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello(#1)
* Screenshot of running http://localhost9999(#2)
* git commands w/ short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorial (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git merge - Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![tomcatworking](img/tomcatworking.PNG)

*Screenshot of running java Hello*:

![javaworld](img/javaworld.PNG)

*Screenshot of index.jsp page*

![indexjsp](img/indexjsp.PNG)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/aga18i/bitbucketstationlocations/ "Bitbucket Station Locations")

*My Online Portfolio:*
[Localhost Link](http://localhost:9999/lis4368/index.jsp "Localhost Link")
