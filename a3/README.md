> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Anthony Alangadan

### Assignment #3 Requirements:

*Three Parts:*

1. Entity Relationship Diagram (ERD)
2. Include at least 10 records per table
3. Chapter Questions (Chs 7-8)

#### README.md file should include the following items:

* Screenshot of ERD that links to the image
* Screenshot of a3 index.jsp
* Link to a3.sql and a3.mwb

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of A3 ERD*:

![A3 ERD Screenshot](img/a3erd.PNG)

*Screenshot of running Tomcat local host A3 JSP File*:

![A3 index.jsp Screenshot](img/a3indexerd.PNG)



#### Links:

*A3.sql File:*
[A3 SQL File Link](docs/a3.sql "A3 SQL File")

*A3.mwb File:*
[A3 MWB File Link](docs/a3.mwb "A3 MWB File")
