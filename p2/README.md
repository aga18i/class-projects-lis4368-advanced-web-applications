> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4368

## Anthony Alangadan

### Project 2 Requirements:

1. Complete the JSP/Servlets web application
2. Use MVC framework
3. Provide CRUD functionality

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Displayed Data
* Screenshot of Form Modification
* Screenshot of Modified Data
* Screenshot of Delete Warning
* Screenshot of Associated Database Charges (Select, Insert, Update, Delete)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Valid User Form Entry*:

![valid user form entry](img/validuserentry.PNG)

*Screenshot of Passed Validation*:

![Passed Validation](img/passed.PNG)

*Screenshot of Displayed Data*:

![displayed data](img/showtables.PNG)



