> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Anthony Alangadan

### Assignment #5 Requirements:

*Sub-Heading:*

1. Provide Java files to connect Server and MySQL
2. Code program to watch for SQL Injection
3. Compile Java files correctly



#### Assignment Screenshots:

*Screenshot of Valid User Form Entry*:

![Valid User Form Entry](img/validuserform.PNG)

*Screenshot of Passed Validation*:

![Passed Validation](img/thanksform.PNG)

*Screenshot of Associated Database Entry*:

![Associated Database Entry](img/data.PNG)



