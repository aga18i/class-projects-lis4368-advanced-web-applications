> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4368

## Anthony Alangadan

### Project 1 Requirements:

*Three Parts:*

1. Add JQuery validation to index.jsp/p1
2. Set min and max to the validation
3. Test and fix the webpage to work properly

#### README.md file should include the following items:

* Screenshot of Main Page
* Screenshot of Failed Validation
* Screenshot of Passed Validation

> This is a blockquote.
> 
>

#### Assignment Screenshots:

*Screenshot of Main Page index.jsp*:

![Main Page Screenshot](img/p1mainpage.PNG)

*Screenshot of Failed Validation*:

![Failed Validation](img/p1failedval.PNG)

*Screenshot of Passed Validation*:

![Passed Validation](img/p1passedval.PNG)


