> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Anthony Alangadan

### Assignment #4 Requirements:

*Sub-Heading:*

1. Implement server side data validation for user entered data
2. Include error page if submitted form is empty


#### Assignment Screenshots:

*Screenshot of error page with empty submit form*:

![Failed Validation Screenshot](img/failedvalidation.PNG)

*Screenshot of successful validation submission form*:

![Successful Validation Screenshot](img/passedvalidation.PNG)




