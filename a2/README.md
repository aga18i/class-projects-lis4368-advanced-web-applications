> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Anthony Alangadan

### Assignment #2 Requirements:

1. Install newer version of MySQL Workbench
2. Write a "Hello-world" Java Servlet
3. Write a Database Servlet
4. Compile Servlet Data

#### README.md file should include the following items:

* Screenshot of http://localhost:9999/lis4368/a2/index.jsp
* Screenshot of http://localhost:9999/hello/index.html
* Screenshot of http://localhost:9999/hello/sayhello
* Screenshot of http://localhost:9999/hello/querybook.html
* Screenshot of the query results

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello/index.html

![localhost hello](img/homepage.PNG)

*Screenshot of http://localhost:9999/hello/sayhello

![localhost sayhello](img/helloworld.PNG)

*Screenshot of http://localhost:9999/hello/querybook.html

![localhost query](img/query.PNG)

*Screenshot of query results

![localhost queryresults](img/queryresponse.PNG)

*Screenshot of index.jsp*

![localhost index.jsp](img/a2index.PNG)


*My Online Portfolio*
[Localhost Link](http://localhost:9999/lis4368/index.jsp "Localhost Link")



