> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications Development

## Anthony Alangadan

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial BitbucketStationLocations
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

       - Install newer version of MySQL Workbench
   
	- Write a "Hello-world" Java Servlet
   
	- Write a Database Servlet
   
	- Compile Servlet Data

3. [A3 README.md](a3/README.md "My A3 README.md file")

       - Create and forward engineer petstore ERD
	
	- Provide screenshots of ERD
	
	- Provide links to .sql and .mwb
	
	- Provide screenshots of a3 index.jsp

4. [A4 README.md](a4/README.md "My A4 README.md file")

       - Implement server side data validation for user entered data
	- Include error page if submitted form is empty


5. [A5 README.md](a5/README.md "My A5 README.md file")

       - Provide Java files to connect Server and MySQL
       - Code program to watch for SQL Injection
	- Compile Java files correctly

6. [P1 README.md](p1/README.md "My P1 README.md file")
       - Add JQuery validation to index.jsp/p1
       - Set min and max to the validation
       - Test and fix the website to work properly

7. [P2 README.md](p2/README.md "My P2 README.md file")
       - Complete the JSP/Servlets web application
       - Use MVC framework
       - Provide CRUD functionality

